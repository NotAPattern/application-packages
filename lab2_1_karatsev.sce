f = figure();
x=[0.15:0.01:0.9]';
f1=(2*sqrt(x))^(-1)+%pi*sin((%pi*x)/2);
plot2d(x, f1,[1],leg="$f(x)/dx$",rect=[0.15,-10,0.9,10],axesflag=5);
xs2eps(gcf(),'lab2_graph2.eps')
