f = figure();
// $y=\sqrt{x}-2\cos{(\frac{\pi}{2}x})}$

Title=uicontrol(f, "style", "text", "string", "$y=\sqrt{x}-2\cos(\frac{\pi}{2}x)$", "position", [230 420 150 40], "fontsize",12);
A=uicontrol(f, "style", "text", "string", "$Left side$", "position", [5 10 110 40], "fontsize",12);
A1=uicontrol(f, "style", "edit", "string", "0.15", "position", [120 10 50 40], "fontsize",12);

B=uicontrol(f, "style", "text", "string", "$Right side$", "position", [180 10 120 40], "fontsize",12);
B1=uicontrol(f, "style", "edit", "string", "0.9", "position", [310 10 40 40], "fontsize",12);

button=uicontrol(f,"style","pushbutton","string","$Show$", "Callback", "Show", "position", [360 10 100 40]);

function Show()
    inputA = evstr(get(A1,'string'));
    inputB = evstr(get(B1,'string'));
    x=[inputA:0.01:inputB]';
    f1=sqrt(x)-2*cos((%pi*x)/2);
    f2=(2*sqrt(x))^(-1)+%pi*sin((%pi*x)/2);
    f3=(%pi^2*cos(%pi*x/2))/2-(4*x^(3/2))^(-1);
    plot2d(x, [f1 f2 f3],[1,2,3],leg="$f(x)$@$f(x)/dx$@$f(x)/dx^2$",rect=[inputA,-10,inputB,10],rect=[inputA,-10,inputB,10],axesflag=5);
    xs2eps(gcf(),'/home/main/Desktop/ppp/lab2_graph1.eps');

    InA= 0.15;
    InB= 0.9;
    fd= mopen('/home/main/Desktop/ppp/report.txt', 'wt')
    mfprintf(fd,"k    ak       bk      abs      f(ak)    f(bk)    f''(bk)\n");
    k=0;
    dif=abs(InB-InA);
    fa = sqrt(InA)-2*cos((%pi*InA)/2);
    fb = sqrt(InB)-2*cos((%pi*InB)/2);
    fsa = (2*sqrt(InB))^(-1)+%pi*sin(%pi*InB/2);
    mfprintf(fd,"%i %8.5f %8.5f %8.5f %8.5f %8.5f %8.5f\n",k,InA,InB,dif,fa,fb,fsa);
    while dif>=0.00001,
        InA = InA - ((dif)/(fb-fa))*fa;
        InB = InB - (1/fsa)*fb;
        fa = sqrt(InA)-2*cos((%pi*InA)/2);
        fb = sqrt(InB)-2*cos((%pi*InB)/2);
        fsa =(2*sqrt(InB))^(-1)+%pi*sin(%pi*InB/2);
        k=k+1;
        dif=InB-InA;
        mfprintf(fd,"%i %8.5f %8.5f %8.5f %8.5f %8.5f %8.5f\n",k,InA,InB,dif,fa,fb,fsa);
    end
    mclose(fd);
endfunction
