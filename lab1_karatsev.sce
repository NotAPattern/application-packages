h=figure(1);


X3o=uicontrol(h, "style", "edit", "string", "5", "position", [50 50 50 50], "fontsize",10);
X2o=uicontrol(h, "style", "edit", "string", "3", "position", [50 100 50 50], "fontsize",10);
X1o=uicontrol(h, "style", "edit", "string", "1", "position", [50 150 50 50], "fontsize",10);

X3=uicontrol(h, "style", "text", "string", "$X+$", "position", [100 50 50 50], "fontsize",10);
X2=uicontrol(h, "style", "text", "string", "$X+$", "position", [100 100 50 50], "fontsize",10);
X1=uicontrol(h, "style", "text", "string", "$X+$", "position", [100 150 50 50], "fontsize",10);

Y3o=uicontrol(h, "style", "edit", "string", "9", "position", [150 50 50 50], "fontsize",10);
Y2o=uicontrol(h, "style", "edit", "string", "6", "position", [150 100 50 50], "fontsize",10);
Y1o=uicontrol(h, "style", "edit", "string", "2", "position", [150 150 50 50], "fontsize",10);

Y3=uicontrol(h, "style", "text", "string", "$Y+$", "position", [200 50 50 50], "fontsize",10);
Y2=uicontrol(h, "style", "text", "string", "$Y+$", "position", [200 100 50 50], "fontsize",10);
Y1=uicontrol(h, "style", "text", "string", "$Y+$", "position", [200 150 50 50], "fontsize",10);

Z3o=uicontrol(h, "style", "edit", "string", "11", "position", [250 50 50 50], "fontsize",10);
Z2o=uicontrol(h, "style", "edit", "string", "1", "position", [250 100 50 50], "fontsize",10);
Z1o=uicontrol(h, "style", "edit", "string", "4", "position", [250 150 50 50], "fontsize",10);

Z3=uicontrol(h, "style", "text", "string", "$Z=$", "position", [300 50 50 50], "fontsize",10);
Z2=uicontrol(h, "style", "text", "string", "$Z=$", "position", [300 100 50 50], "fontsize",10);
Z1=uicontrol(h, "style", "text", "string", "$Z=$", "position", [300 150 50 50], "fontsize",10);

r1 = uicontrol(h, "style", "edit", "string", "12", "position", [350 50 50 50], "fontsize",10);
r2 = uicontrol(h, "style", "edit", "string", "14", "position", [350 100 50 50], "fontsize",10);
r3 = uicontrol(h, "style", "edit", "string", "7", "position", [350 150 50 50], "fontsize",10);





button = uicontrol(h,"style","pushbutton","string","$Run$", "Callback", "Solve", "position", [410 100 70 50]);
resZ = uicontrol(h, "style", "text", "string","$Z=$", "position", [500 50 100 50], "fontsize",10);
resY = uicontrol(h, "style", "text", "string","$Y=$", "position", [500 100 100 50], "fontsize",10);
resX = uicontrol(h, "style", "text", "string","$X=$", "position", [500 150 100 50], "fontsize",1);

function Solve()
   X1Var = evstr(get(X1o, 'string'));
   Y1Var = evstr(get(Y1o, 'string'));
   Z1Var = evstr(get(Z1o, 'string'));
   
   X2Var = evstr(get(X2o, 'string'));
   Y2Var = evstr(get(Y2o, 'string'));
   Z2Var = evstr(get(Z2o, 'string'));
   
   X3Var = evstr(get(X3o, 'string'));
   Y3Var = evstr(get(Y3o, 'string'));
   Z3Var = evstr(get(Z3o, 'string'));
   
   Val1Var = evstr(get(r1, 'string'));
   Val2Var = evstr(get(r2, 'string'));
   Val3Var = evstr(get(r3, 'string'));
   
   
   arr = [X1Var Y1Var Z1Var;X2Var Y2Var Z2Var;X3Var Y3Var Z3Var;]
   eqArr = [Val1Var;Val2Var;Val3Var]
   b = inv(arr);
   res = b*eqArr;
   
   set(resX, 'string',"X=" + string(res(1)));
   set(resY, 'string',"Y=" + string(res(2)));
   set(resZ, 'string',"Z=" + string(res(3)));
endfunction
