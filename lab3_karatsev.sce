f = figure();
plot2d(0,0,1,rect=[15,1,18,1.3], frameflag=3)

function [result] = calcL(array, current, pLagrange)
    result = 1;
    for i=1:length(array)
        if current <> i then
            result = result * (pLagrange - array(i)) / (array(current) - array(i));
        end
    end
endfunction

function [result] = interL(array, arrayL)
    result = 0;
    for i = 1:length(arrayL)
        result = result + array(i) * arrayL(i); 
    end
endfunction

X = [15.000000 15.9000000 16.800000 17.700000];
Y = [1.1760913 1.2013971 1.2253093 1.2479733];

point = 15.6000000;
arrayLagrange = zeros(1, 4);
polynom = poly(0, 'x');

for i = 1:length(X)
    arrayLagrange(i) = calcL(X, i, polynom);
end

resultLagrange = interL(Y, arrayLagrange);
disp(resultLagrange);
printf("\n");

xpoly(X,Y,"marks");
e=gce();
set(e,"mark_style",4);
plot(X, horner(polynom,Y));
xs2eps(gcf(),'/home/main/Desktop/ppp/lab3_graph1.eps');

arrayLagrange = zeros(1, 4);
for i = 1:length(X)
    arrayLagrange(i) = calcL(X, i, point);
    if (i == 1) then printf("\n");
    end
    printf("F(%i) = %s\n", X(i), string(arrayLagrange(i)));
end

printf("\n");
resultLagrange = interL(Y, arrayLagrange);
printf("result = %s, when x = %s", string(resultLagrange), string(point));
